package com.ssh.webssh.service;

import com.ssh.webssh.pojo.FileDTO;
import com.ssh.webssh.pojo.LoginInfo;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.socket.WebSocketSession;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface WebSSHService {

    /**
     * 初始化ssh连接
     * @param session
     */
    void initConnection(WebSocketSession session);

    /**
     * 处理客户段发的数据
     * @param buffer
     * @param session
     */
    void recvHandle(String buffer, WebSocketSession session);

    /**
     * 数据写回前端 for websocket
     * @param session
     * @param buffer
     * @throws IOException
     */
    void sendMessage(WebSocketSession session, byte[] buffer) throws IOException;

    /**
     * 关闭连接
     * @param session
     */
    void close(WebSocketSession session);

    /**
     * 连接ftp
     * @param session
     * @param loginInfo
     * @throws Exception
     */
    void getFTPConnect(HttpSession session, LoginInfo loginInfo) throws Exception;

    /**
     * 获取文件列表
     * @param session
     * @param path
     * @return
     */
    List<FileDTO> listFiles(HttpSession session, String path) throws Exception ;

    /**
     * 上传文件
     * @param session
     * @param uploadFile
     * @param targetDir
     */
    void put(HttpSession session, MultipartFile uploadFile, String targetDir) throws Exception ;

    /**
     * 下载文件
     * @param session
     * @param fullFileName
     * @return
     */
    InputStream down(HttpSession session, String fullFileName) throws Exception;

    /**
     * 删除非文件夹
     * @param session
     * @param fullFileName
     */
    void deleteSingleFile(HttpSession session, String fullFileName) throws Exception;

    /**
     * 断开连接
     * @param session
     */
    void disConnect(HttpSession session) throws Exception;
}
