package com.ssh.webssh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSshApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebSshApplication.class);
    }
}
