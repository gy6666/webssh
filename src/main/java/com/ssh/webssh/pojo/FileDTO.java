package com.ssh.webssh.pojo;

/**
 * 文件上传传输对象
 */
public class FileDTO {

    private String fileName;
    private String fileSize;
    private Boolean isDir;
    private String fullFileName;
    private String mtime;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public Boolean getIsDir() {
        return isDir;
    }

    public void setIsDir(Boolean dir) {
        isDir = dir;
    }

    public String getFullFileName() {
        return fullFileName;
    }

    public void setFullFileName(String fullFileName) {
        this.fullFileName = fullFileName;
    }

    public String getMtime() {
        return mtime;
    }

    public void setMtime(String mtime) {
        this.mtime = mtime;
    }
}
